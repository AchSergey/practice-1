SELECT d.name, SUM(ct.amount) FROM employees em  
 JOIN executor ex ON ex.tab_no = em.id
 JOIN department d ON d.id = em.department_id
 JOIN contract ct ON ct.id = ex.contract_id
 GROUP BY d.name
 ORDER BY SUM(ct.amount)
 DESC
 LIMIT 1