SELECT c.customer_name, ROUND(AVG(ct.amount), 0) FROM customer c 
 JOIN contract ct ON ct.customer_id = c.id
 GROUP BY c.customer_name
 ORDER BY ROUND(AVG(ct.amount),0)
 ASC
 LIMIT 1
